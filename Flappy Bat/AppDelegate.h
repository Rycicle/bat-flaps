//
//  AppDelegate.h
//  Flappy Bat
//
//  Created by Ryan Salton on 09/02/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, assign) BOOL adShowing;

@end
