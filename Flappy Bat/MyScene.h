//
//  MyScene.h
//  Flappy Bat
//

//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Player.h"
#import "Wall.h"
#import "Switch.h"
#import "SettingIcon.h"
#import <GameKit/GameKit.h>

@interface MyScene : SKScene <SKPhysicsContactDelegate, SwitchDelegate, SettingIconDelegate>

@property (nonatomic, strong) Player *player;
@property (nonatomic, strong) SKSpriteNode *floor;

@end
