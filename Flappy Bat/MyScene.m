//
//  MyScene.m
//  Flappy Bat
//
//  Created by Ryan Salton on 09/02/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "MyScene.h"

#define EDGE_BOUNDS_TOP 100
#define EDGE_BOUNDS_BOTTOM 300
#define WALL_GAP 130
#define WALL_WIDTH 40
#define PLAYER_SIZE 50
#define PLAYER_POSITION 70
#define FLOOR_HEIGHT 30

static const uint32_t floorCategory = 0x1 << 1;
static const uint32_t playerCategory = 0x1 << 2;
static const uint32_t wallCategory = 0x1 << 3;
static const uint32_t particleCategory = 0x1 << 4;

@import AVFoundation;

@implementation MyScene {
    int wallTimer;
    int snoreTimer;
    int score;
    NSMutableArray *wallArray;
    NSMutableArray *scorePotentialArray;
    NSMutableArray *particleArray;
    NSMutableArray *textures;
    SKSpriteNode *bg;
    SKSpriteNode *stalagmiteLayer;
    SKSpriteNode *stalagmiteLayer2;
    SKSpriteNode *floorBg;
    SKSpriteNode *floorBg2;
    SKSpriteNode *title;
    SKSpriteNode *begin;
    SKSpriteNode *ledge;
    SKSpriteNode *restart;
    SKSpriteNode *deadMessage;
    SKSpriteNode *topScore;
    SettingIcon *settingsSoundIcon;
    SettingIcon *settingsMusicIcon;
    SKSpriteNode *gameCenterIcon;
    SKSpriteNode *settingsPanel;
    SKLabelNode *topScoreLabel;
    SKLabelNode *scoreLabel;
    SKAction *floorScrollAction;
    SKAction *stalagmiteScrollAction;
    SKAction *playerAnimation;
    SKAction *_squeakSound;
    SKAction *_flapSound;
    AVAudioPlayer *_snoreMusicPlayer;
    AVAudioPlayer *_backgroundMusicPlayer;
    Switch *musicSwitch;
    Switch *SFXSwitch;
    BOOL started;
    BOOL dead;
    BOOL collided;
    BOOL musicOn;
    BOOL soundOn;
}

-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        
//        [self playBackgroundMusic:@"Denizens.mp3"];
        [self playBackgroundMusic:@"Denizens.mp3"];
        _squeakSound = [SKAction playSoundFileNamed:@"Squeak.mp3" waitForCompletion:NO];
        _flapSound = [SKAction playSoundFileNamed:@"Flap.mp3" waitForCompletion:NO];
        
        started = NO;
        
        self.backgroundColor = [SKColor yellowColor];
        wallArray = [[NSMutableArray alloc] init];
        particleArray = [[NSMutableArray alloc] init];
        scorePotentialArray = [[NSMutableArray alloc] init];
        self.physicsWorld.contactDelegate = self;
        
        bg = [SKSpriteNode spriteNodeWithImageNamed:@"bg"];
        bg.colorBlendFactor = 1.0;
        [bg setScale:0.6];
        bg.position = CGPointMake(self.size.width/2, self.size.height/2);
        [self addChild:bg];
        
        stalagmiteLayer = [SKSpriteNode spriteNodeWithImageNamed:@"stalagmite"];
        stalagmiteLayer.anchorPoint = CGPointZero;
        [stalagmiteLayer setScale:0.5];
        stalagmiteLayer.position = CGPointMake(0, 100);
        
        stalagmiteLayer.colorBlendFactor = 1.0;
        [self addChild:stalagmiteLayer];
        
        stalagmiteLayer2 = [SKSpriteNode spriteNodeWithImageNamed:@"stalagmite"];
        stalagmiteLayer2.anchorPoint = CGPointZero;
        [stalagmiteLayer2 setScale:0.5];
        stalagmiteLayer2.position = CGPointMake(stalagmiteLayer.size.width, 100);
        
        stalagmiteLayer2.colorBlendFactor = 1.0;
        [self addChild:stalagmiteLayer2];
        
        wallTimer = 0;
        snoreTimer = 0;
        
        floorBg = [SKSpriteNode spriteNodeWithImageNamed:@"rocky"];
        [floorBg setScale:0.8];
        floorBg.anchorPoint = CGPointZero;
        floorBg.position = CGPointMake(0, 100.0);
        floorBg.zPosition = 100;
        [self addChild:floorBg];
        
        floorBg2 = [SKSpriteNode spriteNodeWithImageNamed:@"rocky"];
        [floorBg2 setScale:0.8];
        floorBg2.anchorPoint = CGPointZero;
        floorBg2.position = CGPointMake(floorBg.size.width, 100.0);
        floorBg2.zPosition = 100;
        [self addChild:floorBg2];
        
        self.floor = [SKSpriteNode spriteNodeWithImageNamed:@"bottombg"];
        self.floor.position = CGPointMake(self.size.width/2, self.floor.size.height/2);
        self.floor.zPosition = 100;
        self.floor.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(self.floor.frame.size.width, self.floor.frame.size.height)];
        self.floor.physicsBody.dynamic = NO;
        self.floor.physicsBody.categoryBitMask = floorCategory;
        self.floor.physicsBody.collisionBitMask = playerCategory;
        [self addChild:self.floor];
        
        SKSpriteNode *ceiling = [SKSpriteNode spriteNodeWithColor:[SKColor clearColor] size:CGSizeMake(self.size.width, 1)];
        ceiling.position = CGPointMake(self.size.width/2, self.size.height);
        ceiling.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(ceiling.frame.size.width, ceiling.frame.size.height)];
        ceiling.physicsBody.dynamic = NO;
        ceiling.physicsBody.categoryBitMask = floorCategory;
        ceiling.physicsBody.collisionBitMask = playerCategory;
        [self addChild:ceiling];
        
        scoreLabel = [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
        scoreLabel.fontSize = 120;
        scoreLabel.fontColor = [SKColor whiteColor];
        scoreLabel.zPosition = 100;
        scoreLabel.hidden = YES;
        [self addChild:scoreLabel];
        
        deadMessage = [SKSpriteNode spriteNodeWithImageNamed:@"deadmessage"];
        deadMessage.position = CGPointMake(self.size.width/2, self.size.height/2 + 120);
        deadMessage.hidden = YES;
        deadMessage.zPosition = 100;
        [self addChild:deadMessage];
        
        topScore = [SKSpriteNode spriteNodeWithImageNamed:@"topscore"];
        topScore.zPosition = 100;
        [self addChild:topScore];
        topScoreLabel = [SKLabelNode labelNodeWithFontNamed:@"Marker Felt"];
        topScoreLabel.fontSize = 30;
        topScoreLabel.fontColor = [SKColor whiteColor];
        NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
        NSString *topScoreData = [userData stringForKey:@"TopScore"];
        if(topScoreData){
            topScoreLabel.text = [NSString stringWithFormat:@"%i", topScoreData.intValue];
        }
        else{
            topScoreLabel.text = @"0";
        }
        topScoreLabel.zPosition = 100;
        
        [self addChild:topScoreLabel];
        
        NSLog(@"%i", [self checkSettingForKey:@"SoundSetting"]);
        
        soundOn = YES;
        musicOn = YES;
        NSString *soundData = [userData stringForKey:@"SoundSetting"];
        NSString *musicData = [userData stringForKey:@"MusicSetting"];
        if(!soundData){
            [self setSettingForKey:@"SoundSetting" withValue:1];
        }
        if(!musicData){
            [self setSettingForKey:@"MusicSetting" withValue:1];
        }
        if([self checkSettingForKey:@"SoundSetting"] == 0){
            soundOn = NO;
        }
        if([self checkSettingForKey:@"MusicSetting"] == 0){
            musicOn = NO;
        }
        
        settingsSoundIcon = [[SettingIcon alloc] initWithImageNamed:@"sound" andState:[self checkSettingForKey:@"SoundSetting"] andID:0];
        settingsSoundIcon.position = CGPointMake(self.size.width-30, self.size.height-30);
        settingsSoundIcon.delegate = self;
        [self addChild:settingsSoundIcon];
        
        settingsMusicIcon = [[SettingIcon alloc] initWithImageNamed:@"music" andState:[self checkSettingForKey:@"MusicSetting"] andID:1];
        settingsMusicIcon.position = CGPointMake(settingsSoundIcon.position.x, settingsSoundIcon.position.y-50);
        settingsMusicIcon.delegate = self;
        [self addChild:settingsMusicIcon];
        
        gameCenterIcon = [[SKSpriteNode alloc] initWithImageNamed:@"gameCenter"];
        gameCenterIcon.name = @"gameCenterIcon";
        gameCenterIcon.position = CGPointMake(settingsSoundIcon.position.x, settingsSoundIcon.position.y-100);
        [self addChild:gameCenterIcon];
        
        [self startScreenSetup];
        
    }
    return self;
}

-(void)iconPressed:(int)settingID withState:(int)state
{
    if(settingID == 0){
        [self setSettingForKey:@"SoundSetting" withValue:state];
        soundOn = (BOOL)state;
    }
    else if(settingID == 1){
        [self setSettingForKey:@"MusicSetting" withValue:state];
        musicOn = (BOOL)state;
    }
}

-(int)checkSettingForKey:(NSString*)key
{
    NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
    NSString *settingData = [userData stringForKey:key];
    int settingValue = settingData.intValue;
    return settingValue;
}

-(void)setSettingForKey:(NSString *)key withValue:(int)settingValue
{
    NSUserDefaults *userValues = [NSUserDefaults standardUserDefaults];
    [userValues setObject:[NSString stringWithFormat:@"%i",settingValue] forKey:key];
}

- (void)playBackgroundMusic:(NSString *)filename
{
    NSError *error;
    NSURL *backgroundMusicURL =
    [[NSBundle mainBundle] URLForResource:filename
                            withExtension:nil];
    _backgroundMusicPlayer =
    [[AVAudioPlayer alloc]
     initWithContentsOfURL:backgroundMusicURL error:&error];
    _backgroundMusicPlayer.numberOfLoops = -1;
    [_backgroundMusicPlayer prepareToPlay];
    
    [self resetSnoring];
    
}

-(void)resetSnoring
{
    NSError *error;
    NSURL *snoreMusicURL =
    [[NSBundle mainBundle] URLForResource:@"Snoring.mp3"
                            withExtension:nil];
    _snoreMusicPlayer =
    [[AVAudioPlayer alloc]
     initWithContentsOfURL:snoreMusicURL error:&error];
    _snoreMusicPlayer.numberOfLoops = -1;
    [_snoreMusicPlayer prepareToPlay];
}

-(void)startScreenSetup
{
    settingsSoundIcon.hidden = NO;
    settingsMusicIcon.hidden = NO;
    gameCenterIcon.hidden = NO;
    
    _backgroundMusicPlayer.volume = 1.0;
    bg.color = [SKColor colorWithRed:87.0/255.0 green:87.0/255.0 blue:60.0/255.0 alpha:1.0];
    stalagmiteLayer.color = [SKColor colorWithRed:87.0/255.0 green:87.0/255.0 blue:60.0/255.0 alpha:1.0];
    stalagmiteLayer2.color = [SKColor colorWithRed:87.0/255.0 green:87.0/255.0 blue:60.0/255.0 alpha:1.0];
//    self.player = [Player spriteNodeWithImageNamed:@"player"];
    self.player = [Player spriteNodeWithImageNamed:@"playersleep1"];
    
    textures = [NSMutableArray arrayWithCapacity:14];
    
    int SNORE1_FRAMES = 25;
    int SNORE2_FRAMES = 1;
    int SNORE3_FRAMES = 45;
    
    for (int i = 1; i < (SNORE1_FRAMES + (SNORE2_FRAMES * 2) + SNORE3_FRAMES + 1); i++) {
        NSString *textureName;
        if(i < SNORE1_FRAMES + 1) {
            textureName = [NSString stringWithFormat:@"playersleep1"];
        }
        else if(i == SNORE1_FRAMES + 1 || i == SNORE1_FRAMES + SNORE2_FRAMES + SNORE3_FRAMES + 1){
            textureName = [NSString stringWithFormat:@"playersleep2"];
        }
        else {
            textureName = [NSString stringWithFormat:@"playersleep3"];
        }
        SKTexture *texture = [SKTexture textureWithImageNamed:textureName];
        [textures addObject:texture];
    }
    playerAnimation = [SKAction animateWithTextures:textures timePerFrame:0.08];
    
    [self.player runAction:[SKAction repeatActionForever:playerAnimation]];
    
    [self.player setScale:0.3];
    self.player.position = CGPointMake((self.size.width / 2) - PLAYER_POSITION, self.size.height/2);
    self.player.zPosition = 5;
    self.player.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:self.player.size.height / 2];
    self.player.physicsBody.dynamic = NO;
    self.player.physicsBody.categoryBitMask = playerCategory;
    self.player.physicsBody.collisionBitMask = floorCategory | wallCategory;
    self.player.physicsBody.contactTestBitMask =  wallCategory;
    [self addChild:self.player];
    
    [scoreLabel setScale:0.33];
    scoreLabel.position = CGPointMake(self.size.width/2, self.size.height - 50);
    
    topScoreLabel.position = CGPointMake(self.size.width/2, 0 + 25 - topScore.size.height);
    topScore.position = CGPointMake(self.size.width/2, 0 - topScore.size.height/2);
    
    title = [SKSpriteNode spriteNodeWithImageNamed:@"title"];
    title.position = CGPointMake(self.size.width/2, self.size.height/2 + 150);
    [title setScale:0.6];
    [self addChild:title];
    
    begin = [SKSpriteNode spriteNodeWithImageNamed:@"begin"];
    begin.position = CGPointMake(self.player.position.x + 100, self.player.position.y);
    [begin setScale:0.4];
    [self addChild:begin];
    
    ledge = [SKSpriteNode spriteNodeWithImageNamed:@"ledge"];
    ledge.position = CGPointMake(self.player.position.x - 30, self.player.position.y - 50);
    [ledge setScale:0.4];
    [self addChild:ledge];
    
    scoreLabel.hidden = YES;
    score = 0;
    [self updateScore];
    
    [self resetSnoring];
    [_snoreMusicPlayer play];
}

-(void)startFloorScrolling
{
    floorScrollAction = [SKAction moveByX:-floorBg.size.width - 10 y:0 duration:7.49];
    [floorBg runAction:floorScrollAction];
    [floorBg2 runAction:floorScrollAction];
    
    stalagmiteScrollAction = [SKAction moveByX:-stalagmiteLayer.size.width - 10 y:0 duration:10];
    [stalagmiteLayer runAction:stalagmiteScrollAction];
    [stalagmiteLayer2 runAction:stalagmiteScrollAction];
}

-(void)stopFloorScrolling
{
    [floorBg removeAllActions];
    [floorBg2 removeAllActions];
    
    [stalagmiteLayer removeAllActions];
    [stalagmiteLayer2 removeAllActions];
}

-(void)showGameCenter
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"showGameCenter" object:nil userInfo:nil];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    
    //if hero touched set BOOL
    if ([node.name isEqualToString:@"gameCenterIcon"]) {
        [self showGameCenter];
        return;
    }
    
    if(dead){
        
        if(wallArray.count > 0){
            for(Wall *wall in wallArray){
                [wall removeFromParent];
            }
        }
        [wallArray removeAllObjects];
        
        if(particleArray.count > 0) {
            for (SKSpriteNode *particle in particleArray) {
                [particle removeFromParent];
            }
        }
        [particleArray removeAllObjects];
        
        [self stopFloorScrolling];
        [restart removeFromParent];
        [self startScreenSetup];
        
        [_backgroundMusicPlayer stop];
        [self resetSnoring];
        [_snoreMusicPlayer play];
        
        deadMessage.hidden = YES;
        started = NO;
        dead = NO;
        collided = NO;
        
        [scoreLabel removeAllActions];
        [scoreLabel setScale:0.33];
        scoreLabel.position = CGPointMake(self.size.width/2, self.size.height - 50);
        
        [topScoreLabel removeAllActions];
        [topScore removeAllActions];
        topScoreLabel.position = CGPointMake(self.size.width/2, 0 + 25 - topScore.size.height);
        topScore.position = CGPointMake(self.size.width/2, 0 - topScore.size.height/2);
    }
    else{
    if(!started){
        [title removeFromParent];
        [begin removeFromParent];
        
        settingsSoundIcon.hidden = YES;
        settingsMusicIcon.hidden = YES;
        gameCenterIcon.hidden = YES;
        
        [scoreLabel removeAllActions];
        [scoreLabel setScale:0.33];
        scoreLabel.position = CGPointMake(self.size.width/2, self.size.height - 50);
        
        [topScoreLabel removeAllActions];
        [topScore removeAllActions];
        topScoreLabel.position = CGPointMake(self.size.width/2, 0 + 25 - topScore.size.height);
        topScore.position = CGPointMake(self.size.width/2, 0 - topScore.size.height/2);
        
        self.player.physicsBody.dynamic = YES;
        
        [_snoreMusicPlayer stop];
        [_backgroundMusicPlayer play];
        
        [textures removeAllObjects];
        SKTexture *texture = [SKTexture textureWithImageNamed:@"player"];
        [textures addObject:texture];
        playerAnimation = [SKAction animateWithTextures:textures timePerFrame:0.08];
        [self.player runAction:[SKAction repeatActionForever:playerAnimation]];
        
        
        started = YES;
        [self startFloorScrolling];
        
        score = 0;
        [scorePotentialArray removeAllObjects];
        [self updateScore];
        scoreLabel.hidden = NO;
        
        
        SKAction *moveLedge = [SKAction moveTo:CGPointMake(-ledge.size.width, ledge.position.y) duration:1.6];
        SKAction *removeLedge = [SKAction removeFromParent];
        
        [ledge runAction:[SKAction sequence:@[moveLedge, removeLedge]]] ;
    }
    
    float xDiff = (((self.size.width / 2) - PLAYER_POSITION) - self.player.position.x)/8;
    
    self.player.physicsBody.velocity = self.physicsBody.velocity;
    [self.player.physicsBody applyImpulse: CGVectorMake(xDiff,28.0)];
    [self flapWings];
        
    SKAction *getUpright = [SKAction rotateToAngle:0 duration:0.5];
    [self.player runAction:getUpright];
    }
    
}

-(void)flapWings
{
//    [self runAction:_flapSound];
    
    [textures removeAllObjects];
    SKTexture *texture = [SKTexture textureWithImageNamed:@"player2"];
    SKTexture *texture2 = [SKTexture textureWithImageNamed:@"player3"];
    [textures addObject:texture];
    [textures addObject:texture2];
    [textures addObject:texture];

    playerAnimation = [SKAction animateWithTextures:textures timePerFrame:0.2];
    [self.player runAction:[SKAction repeatActionForever:playerAnimation]];
    
    SKAction *waitAction = [SKAction waitForDuration:0.4];
    SKAction *resetPlayer = [SKAction customActionWithDuration:0 actionBlock:^(SKNode *node, CGFloat elapsedTime) {
        [textures removeAllObjects];
        SKTexture *texture = [SKTexture textureWithImageNamed:@"player"];
        [textures addObject:texture];
        playerAnimation = [SKAction animateWithTextures:textures timePerFrame:0.2];
        [self.player runAction:[SKAction repeatActionForever:playerAnimation]];
    }];
    [self runAction:[SKAction sequence:@[waitAction, resetPlayer]]];
    
}

-(void)createWall
{
//    Wall *topWall = [Wall spriteNodeWithColor:[SKColor greenColor] size:CGSizeMake(WALL_WIDTH, self.size.height)];
    Wall *topWall = [Wall spriteNodeWithImageNamed:@"wall"];
    [topWall setScale:0.4];
    topWall.position = CGPointMake(self.size.width + topWall.size.width, [self randomIntBetween:EDGE_BOUNDS_BOTTOM and:self.size.height - EDGE_BOUNDS_TOP] + topWall.size.height/2);
    topWall.zPosition = 5;
    topWall.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(topWall.frame.size.width - 5, topWall.frame.size.height - 5)];
    topWall.physicsBody.dynamic = NO;
    topWall.physicsBody.categoryBitMask = wallCategory;
    topWall.physicsBody.collisionBitMask = playerCategory;
    [scorePotentialArray addObject:topWall];
    [wallArray addObject:topWall];
    [self addChild:topWall];
    
//    Wall *bottomWall = [Wall spriteNodeWithColor:[SKColor greenColor] size:CGSizeMake(WALL_WIDTH, self.size.height)];
    Wall *bottomWall = [Wall spriteNodeWithImageNamed:@"wall"];
    [bottomWall setScale:0.4];
    bottomWall.position = CGPointMake(topWall.position.x, topWall.position.y - bottomWall.size.height - WALL_GAP);
    bottomWall.zPosition = 5;
    bottomWall.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(bottomWall.frame.size.width - 5, bottomWall.frame.size.height - 5)];
    bottomWall.physicsBody.dynamic = NO;
    bottomWall.physicsBody.categoryBitMask = wallCategory;
    bottomWall.physicsBody.collisionBitMask = playerCategory;
    [wallArray addObject:bottomWall];
    [self addChild:bottomWall];
    
    SKAction *moveTopLeftAction = [SKAction moveTo:CGPointMake(-topWall.size.width, topWall.position.y) duration:4.0];
    SKAction *moveBottomLeftAction = [SKAction moveTo:CGPointMake(-bottomWall.size.width, bottomWall.position.y) duration:4.0];
    SKAction *removeWallAction = [SKAction customActionWithDuration:0 actionBlock:^(SKNode *node, CGFloat elapsedTime) {
        [SKAction removeFromParent];
        [wallArray removeObjectAtIndex:0];
    }];
    
    [topWall runAction:[SKAction sequence:@[moveTopLeftAction, removeWallAction]]];
    [bottomWall runAction:[SKAction sequence:@[moveBottomLeftAction, removeWallAction]]];
    
//    NSLog(@"%i", (int)wallArray.count);
    
}

-(void)didBeginContact:(SKPhysicsContact *)contact
{
    SKAction *changeBG = [SKAction colorizeWithColor:[SKColor redColor] colorBlendFactor:1.0 duration:1.0];
    
//    SKAction *changeBG = [SKAction customActionWithDuration:0.5 actionBlock:^(SKNode *node, CGFloat elapsedTime) {
//       self.backgroundColor = [SKColor redColor];
//    }];
    [self killPlayer];
    collided = YES;
    SKAction *setDeath = [SKAction customActionWithDuration:0 actionBlock:^(SKNode *node, CGFloat elapsedTime) {
        restart = [SKSpriteNode spriteNodeWithImageNamed:@"restart"];
        restart.position = CGPointMake(self.size.width/2, self.size.height/2 - 80);
        [restart setScale:0.4];
        restart.zPosition = 100;
        restart.alpha = 0.0;
        dead = YES;
        [self addChild:restart];
        deadMessage.hidden = NO;
        deadMessage.alpha = 0.0;
        
        NSUserDefaults *userValues = [NSUserDefaults standardUserDefaults];
        NSString *myTopScore = [userValues stringForKey:@"TopScore"];
        if(myTopScore){
            if (myTopScore.intValue < score) {
                [userValues setObject:[NSString stringWithFormat:@"%i",score] forKey:@"TopScore"];
                topScoreLabel.text = [NSString stringWithFormat:@"%i", score];
            }
        }
        else{
            [userValues setObject:[NSString stringWithFormat:@"%i",score] forKey:@"TopScore"];
            topScoreLabel.text = myTopScore;
        }
        [self postScoreToGameCenter:[[userValues stringForKey:@"TopScore"] intValue]];
        
        SKAction *showLabel = [SKAction fadeInWithDuration:0.5];
        SKAction *moveScore = [SKAction moveTo:CGPointMake(self.size.width/2, self.size.height/2 - 30) duration:0.5];
        SKAction *inflateScore = [SKAction scaleTo:1.0 duration:0.5];
        
        NSInteger topScoreAdjust = 0;
        if(((AppDelegate*)[[UIApplication sharedApplication] delegate]).adShowing){
            topScoreAdjust = 45;
        }
        
        SKAction *showTopScore = [SKAction moveTo:CGPointMake(self.size.width/2, (topScore.size.height * 0.5) + topScoreAdjust) duration:0.5];
        SKAction *showtopScoreLabel = [SKAction moveTo:CGPointMake(self.size.width/2, 25 + topScoreAdjust) duration:0.5];
        [restart runAction:showLabel];
        [deadMessage runAction:showLabel];
        [scoreLabel runAction:[SKAction group:@[moveScore, inflateScore]]];
        [topScore runAction:showTopScore];
        [topScoreLabel runAction:showtopScoreLabel];
        
    }];
    [stalagmiteLayer runAction:changeBG];
    [stalagmiteLayer2 runAction:changeBG];
    [bg runAction:[SKAction sequence:@[changeBG, setDeath]]];
}

-(void)killPlayer
{
    // Create particles
    // Top row
    for (int i = 0; i < 4; i++) {
        [self createParticleAt:CGPointMake(self.player.position.x - 20 + (i * 10), self.player.position.y + 30)];
    }
    
    // Middle rows
    for(int x = 0; x < 4; x++){
        for (int i = 0; i < 5; i++) {
            [self createParticleAt:CGPointMake(self.player.position.x - 25 + (i * 10), self.player.position.y - 20 + (x * 10))];
        }
    }
    
    // Bottom row
    for (int i = 0; i < 4; i++) {
        [self createParticleAt:CGPointMake(self.player.position.x - 20 + (i * 10), self.player.position.y - 30)];
    }
    
    SKSpriteNode *explodeForce = [SKSpriteNode spriteNodeWithColor:[SKColor clearColor] size:CGSizeMake(15, 15)];
    explodeForce.position = CGPointMake(self.player.position.x, self.player.position.y - 50);
    explodeForce.physicsBody.velocity = self.physicsBody.velocity;
    
    explodeForce.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:explodeForce.size.height / 2];
    explodeForce.physicsBody.dynamic = YES;
    explodeForce.physicsBody.categoryBitMask = particleCategory;
    explodeForce.physicsBody.collisionBitMask = particleCategory;
    
    [particleArray addObject:explodeForce];
    [self addChild:explodeForce];
    
    [explodeForce.physicsBody applyImpulse: CGVectorMake(0,15.0)];
    
    [self.player removeFromParent];
}

-(void)createParticleAt:(CGPoint)point
{
//    SKSpriteNode *particle = [SKSpriteNode spriteNodeWithColor:[SKColor blackColor] size:CGSizeMake(10, 10)];
    SKSpriteNode *particle = [SKSpriteNode spriteNodeWithImageNamed:@"player"];
    [particle setScale:0.1];
    particle.position = point;
    
    particle.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:particle.size.height / 2];
    particle.physicsBody.dynamic = YES;
    particle.physicsBody.categoryBitMask = particleCategory;
    particle.physicsBody.collisionBitMask = floorCategory | particleCategory;
    particle.physicsBody.restitution = 0.5;
    [particleArray addObject:particle];
    [self addChild:particle];
}

-(void)createDebrisOfType:(int)debrisType
{
    int atX = [self randomIntBetween:20 and:self.size.width];
    
    SKSpriteNode *debris;
    if(debrisType == 0){
        debris = [SKSpriteNode spriteNodeWithImageNamed:@"drip"];
        [debris setScale:0.3];
        debris.colorBlendFactor = 1.0;
        debris.color = [SKColor colorWithRed:115.0/255.0 green:185.0/255.0 blue:255.0/255.0 alpha:1.0];
    }
    else if (debrisType == 1){
        debris = [SKSpriteNode spriteNodeWithImageNamed:@"debris"];
        [debris setScale:0.4];
    }
    debris.position = CGPointMake(atX, self.size.height + 20);
    debris.zPosition = 1;
    [self addChild:debris];
    
    SKAction *debrisDrop = [SKAction moveByX:-50 y:-(self.size.height + 20) duration:1.2];
    SKAction *debrisRemove = [SKAction removeFromParent];
    [debris runAction:[SKAction sequence:@[debrisDrop, debrisRemove]]];
}

- (void)updateScore
{
    scoreLabel.text = [NSString stringWithFormat:@"%i", score];
}

-(void)snore
{
    SKLabelNode *zzz = [SKLabelNode labelNodeWithFontNamed:@"Felt Marker"];
    zzz.text = @"Z";
    zzz.fontColor = [SKColor whiteColor];
    zzz.fontSize = 20;
    zzz.position = self.player.position;
    zzz.zPosition = 98;
    [self addChild:zzz];
    
    SKAction *snoreMove = [SKAction moveByX:0 y:30 duration:1.0];
    SKAction *snoreFade = [SKAction fadeOutWithDuration:1.0];
    SKAction *snoreRemove = [SKAction removeFromParent];
    [zzz runAction:[SKAction sequence:@[[SKAction group:@[snoreMove, snoreFade]], snoreRemove]]];
}

- (int)randomIntBetween:(int)smallNumber and:(int)bigNumber
{
    int lowerBound = smallNumber;
    int upperBound = bigNumber;
    int rndValue = lowerBound + arc4random() % (upperBound - lowerBound);
    return rndValue;
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    
    if(musicOn){
        _backgroundMusicPlayer.volume = 1;
    }
    else{
        _backgroundMusicPlayer.volume = 0;
    }
    if(soundOn){
        _snoreMusicPlayer.volume = 1;
    }
    else{
        _snoreMusicPlayer.volume = 0;
    }
    
    if(started && !dead){
        wallTimer++;
        if (wallTimer % 100 == 0) {
//        NSLog(@"%i", wallTimer);
            [self createWall];
        }
        if (wallTimer % 180 == 0) {
            [self createDebrisOfType:0];
        }
        if (wallTimer % 160 == 0) {
            [self createDebrisOfType:1];
        }
    }
    
    if(scorePotentialArray.count > 0){
        SKSpriteNode *checkWall = scorePotentialArray[0];
        if(checkWall.position.x <= self.player.position.x - 20 && !collided) {
            score++;
            [self updateScore];
            if(started && soundOn){
                [self runAction:_squeakSound];
            }
            [scorePotentialArray removeObjectAtIndex:0];
        }
    }
    
    if(dead && started) {
        if(_backgroundMusicPlayer.volume > 0){
            _backgroundMusicPlayer.volume = _backgroundMusicPlayer.volume - 0.005;
        }
        if(_backgroundMusicPlayer <= 0){
            [_backgroundMusicPlayer stop];
        }
    }
    
    if(!started) {
        snoreTimer++;
        if(snoreTimer % 100 == 0){
            [self snore];
        }
    }
    
    if(floorBg.position.x <= -floorBg.size.width) {
        floorBg.position = CGPointMake(floorBg2.size.width, floorBg2.position.y);
        [floorBg removeAllActions];
        [floorBg2 removeAllActions];
        [floorBg runAction:floorScrollAction];
        [floorBg2 runAction:floorScrollAction];
    }
    if(floorBg2.position.x <= -floorBg2.size.width) {
        floorBg2.position = CGPointMake(floorBg.size.width, floorBg.position.y);
        [floorBg removeAllActions];
        [floorBg2 removeAllActions];
        [floorBg runAction:floorScrollAction];
        [floorBg2 runAction:floorScrollAction];
    }
    
    if(stalagmiteLayer.position.x <= -stalagmiteLayer.size.width) {
        stalagmiteLayer.position = CGPointMake(stalagmiteLayer2.size.width, stalagmiteLayer2.position.y);
        [stalagmiteLayer removeAllActions];
        [stalagmiteLayer2 removeAllActions];
        [stalagmiteLayer runAction:stalagmiteScrollAction];
        [stalagmiteLayer2 runAction:stalagmiteScrollAction];
    }
    if(stalagmiteLayer2.position.x <= -stalagmiteLayer2.size.width) {
        stalagmiteLayer2.position = CGPointMake(stalagmiteLayer.size.width, stalagmiteLayer.position.y);
        [stalagmiteLayer removeAllActions];
        [stalagmiteLayer2 removeAllActions];
        [stalagmiteLayer runAction:stalagmiteScrollAction];
        [stalagmiteLayer2 runAction:stalagmiteScrollAction];
    }
    
    //NSLog(@"POSITION: %f WIDTH: %f", stalagmiteLayer2.position.x, stalagmiteLayer2.size.width);
}

- (void)postScoreToGameCenter:(NSInteger)scoreValue
{
    GKScore *gkScore = [[GKScore alloc] initWithLeaderboardIdentifier:@"BatFlaps"];
    gkScore.value = scoreValue;
    
    [GKScore reportScores:@[gkScore] withCompletionHandler:^(NSError *error) {
        
        NSLog(@"%@", error);
        //            [self animateGameCenterButton];
        
    }];
    
}

@end
