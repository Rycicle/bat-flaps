//
//  SettingIcon.h
//  Flappy Bat
//
//  Created by Ryan Salton on 13/02/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@class SettingIcon;
@protocol SettingIconDelegate <NSObject>
-(void)iconPressed:(int)settingID withState:(int)state;
@end

@interface SettingIcon : SKSpriteNode

@property (nonatomic, assign) id <SettingIconDelegate> delegate;
@property (nonatomic) int state;
@property (nonatomic) int settingID;

-(id)initWithImageNamed:(NSString *)name andState:(int)state andID:(int)settingID;

@end
