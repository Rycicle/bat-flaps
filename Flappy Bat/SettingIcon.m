//
//  SettingIcon.m
//  Flappy Bat
//
//  Created by Ryan Salton on 13/02/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "SettingIcon.h"

@implementation SettingIcon

-(id)initWithImageNamed:(NSString *)name andState:(int)state andID:(int)settingID
{
    if (self = [super initWithImageNamed:name]) {
        self.userInteractionEnabled = YES;
        self.state = state;
        self.settingID = settingID;
        self.colorBlendFactor = 1.0;
        self.color = [SKColor whiteColor];
        [self checkState];
    }
    return self;
}

-(void)checkState
{
    if(self.state == 0){
        self.alpha = 0.4;
    }
    else {
        self.alpha = 1.0;
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(self.state == 0){
        self.state = 1;
    }
    else {
        self.state = 0;
    }
    [self checkState];
    [self.delegate iconPressed:self.settingID withState:self.state];
}

@end
