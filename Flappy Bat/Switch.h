//
//  Switch.h
//  Flappy Bat
//
//  Created by Ryan Salton on 12/02/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@class Switch;
@protocol SwitchDelegate <NSObject>
-(void)switchPressed;
@end

@interface Switch : SKSpriteNode

@property (nonatomic, strong) SKSpriteNode *spriteImage;
@property (nonatomic, assign) id <SwitchDelegate> delegate;
@property (nonatomic) int state;

-(id)initWithImageNamed:(NSString *)name andState:(int)state;

@end
