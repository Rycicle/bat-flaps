//
//  Switch.m
//  Flappy Bat
//
//  Created by Ryan Salton on 12/02/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "Switch.h"

@implementation Switch

-(id)initWithImageNamed:(NSString *)name andState:(int)state
{
    if (self = [super initWithImageNamed:name]) {
        self.userInteractionEnabled = YES;
        self.state = state;
        [self checkState];
        
        NSLog(@"%f", self.size.width);
    }
    return self;
}

-(void)checkState
{
//    if (self.spriteImage){
//        [self.spriteImage removeFromParent];
//    }
//    if(self.state == 0){
//        self.spriteImage = [SKSpriteNode spriteNodeWithImageNamed:@"switchoff"];
//    }
//    else {
//        self.spriteImage = [SKSpriteNode spriteNodeWithImageNamed:@"switchon"];
//    }
//    
//    [self addChild:self.spriteImage];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"SWITCH PRESSED");
    if(self.state == 0){
        self.state = 1;
    }
    else {
        self.state = 0;
    }
    [self.delegate switchPressed];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    
    //Do stuff here...
}

@end
