//
//  ViewController.h
//  Flappy Bat
//

//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import <iAd/iAd.h>
#import <GameKit/GameKit.h>

@interface ViewController : UIViewController <ADBannerViewDelegate, GKGameCenterControllerDelegate>

@end
