//
//  ViewController.m
//  Flappy Bat
//
//  Created by Ryan Salton on 09/02/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "ViewController.h"
#import "MyScene.h"

@implementation ViewController {
    ADBannerView *adBannerView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Configure the view.
    SKView * skView = (SKView *)self.view;
//    skView.showsFPS = YES;
//    skView.showsNodeCount = YES;
    
    // Create and configure the scene.
    SKScene * scene = [MyScene sceneWithSize:skView.bounds.size];
    scene.scaleMode = SKSceneScaleModeAspectFill;
    
    // Present the scene.
    [skView presentScene:scene];
     [self authenticateGameCenterPlayer];
    [self createAdBannerView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showGameCenter) name:@"showGameCenter" object:nil];
}

-(void)showGameCenter
{
    GKGameCenterViewController *gameCenterController = [[GKGameCenterViewController alloc] init];
    gameCenterController.gameCenterDelegate = self;
    gameCenterController.viewState = GKGameCenterViewControllerStateDefault;
    gameCenterController.leaderboardIdentifier = @"BatFlaps";
    [self presentViewController:gameCenterController animated: YES completion:nil];
}

-(void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)presentGCViewController:(UIViewController *)viewController
{
    [self presentViewController:viewController animated:YES completion:nil];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)createAdBannerView {
    if (!adBannerView)
    {
        adBannerView = [[ADBannerView alloc] initWithAdType:ADAdTypeBanner];
        adBannerView.delegate = self;
        adBannerView.frame = CGRectMake(0, 0, self.view.bounds.size.width, adBannerView.frame.size.height);
        [self.view addSubview:adBannerView];
    }
    
    adBannerView.center = CGPointMake(self.view.bounds.size.width * 0.5, self.view.frame.size.height + adBannerView.frame.size.height * 0.5);
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"adShowing" object:nil userInfo:nil];
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    appDelegate.adShowing = YES;
    
    [UIView animateWithDuration:0.3 animations:^{
        adBannerView.center = CGPointMake(self.view.bounds.size.width * 0.5, self.view.bounds.size.height - (adBannerView.frame.size.height * 0.5));
    }];
}

-(void)removeAdBanner
{
    [UIView animateWithDuration:0.3 animations:^{
        adBannerView.center = CGPointMake(self.view.bounds.size.width * 0.5, self.view.frame.size.height + adBannerView.frame.size.height * 0.5);
    } completion:^(BOOL finished) {
        [adBannerView removeFromSuperview];
        adBannerView = nil;
    }];
}

- (void)authenticateGameCenterPlayer
{
    if ([GKLocalPlayer localPlayer].isAuthenticated)
    {
        [self getScoreFromGameCenter];
    }
    else
    {
        [GKLocalPlayer localPlayer].authenticateHandler = ^(UIViewController *viewController, NSError *error){
            
            if (viewController != nil)
            {
                [self presentGCViewController:viewController];
            }
            else if ([GKLocalPlayer localPlayer].isAuthenticated)
            {
                [self getScoreFromGameCenter];
            }
            
        };
    }
}

- (void)getScoreFromGameCenter
{
    //    GKLeaderboard *leaderboard = [[GKLeaderboard alloc] initWithPlayerIDs:@[[GKLocalPlayer localPlayer].playerID]];
    //    leaderboard.identifier = LEADERBOARD_IDENTIFIER_BEST_TIMES;
    //
    //    [leaderboard loadScoresWithCompletionHandler:^(NSArray *scores, NSError *error) {
    //
    //        if (scores.count == 1)
    //        {
    //            GKScore *score = scores[0];
    //
    //            CGFloat leaderboardTime = score.value / 10.0;
    //            CGFloat bestTime = [[NSUserDefaults standardUserDefaults] floatForKey:USER_DEFAULTS_BEST_TIME];
    //
    //            if (bestTime > leaderboardTime)
    //            {
    //                [self postScoreToGameCenter];
    //            }
    //            else
    //            {
    //                [self animateGameCenterButton];
    //            }
    //        }
    //        else
    //        {
    //            [self postScoreToGameCenter];
    //        }
    //
    //    }];
}

@end
