//
//  Wall.h
//  Flappy Bat
//
//  Created by Ryan Salton on 09/02/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Wall : SKSpriteNode

@end
